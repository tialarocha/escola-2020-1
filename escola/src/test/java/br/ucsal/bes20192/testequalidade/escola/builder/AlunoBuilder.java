package br.ucsal.bes20192.testequalidade.escola.builder;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

public class AlunoBuilder {
	
	private static final Integer MATRICULA_DEFAULT = 15005678;
	private static final String NOME_DEFAULT = "João Pedro";
	private static final SituacaoAluno SITUACAO_ALUNO_DEFAULT = SituacaoAluno.ATIVO;
	private static final Integer ANO_NASCIMENTO_DEFAULT = 1990;
	
	private Integer matricula = MATRICULA_DEFAULT;
	private String nome = NOME_DEFAULT;
	private SituacaoAluno siruacaoAluno = SITUACAO_ALUNO_DEFAULT;
	private Integer anoNascimento = ANO_NASCIMENTO_DEFAULT;
	
	private AlunoBuilder() {
		
	}
	
	public static AlunoBuilder umAluno() {
		return new AlunoBuilder();
	}
	
	public AlunoBuilder comMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}
	
	public AlunoBuilder deNome(String nome) {
		this.nome = nome;
		return this;
	}
	
	public AlunoBuilder comSituacao(SituacaoAluno siruacaoAluno) {
		this.siruacaoAluno = siruacaoAluno;
		return this;
	}
	
	public AlunoBuilder nascidoEm(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
		return this;
	}
	
	public AlunoBuilder mas(){
        return umAluno().comMatricula(matricula).deNome(nome)
                .comSituacao(siruacaoAluno).nascidoEm(anoNascimento);
    }

	
	public Aluno build() {
		Aluno aluno = new Aluno();
		aluno.setMatricula(matricula);
		aluno.setNome(nome);
		aluno.setSituacao(siruacaoAluno);
		aluno.setAnoNascimento(anoNascimento);
		return aluno;
	}
	
	public AlunoDAO umAlunoDAO(Aluno aluno){
        AlunoDAO alunoDao = new AlunoDAO();
        aluno.setMatricula(matricula);
        aluno.setNome(nome);
        aluno.setSituacao(siruacaoAluno);
        aluno.setAnoNascimento(anoNascimento);
        alunoDao.salvar(aluno);
        return alunoDao;
    }

	
}
