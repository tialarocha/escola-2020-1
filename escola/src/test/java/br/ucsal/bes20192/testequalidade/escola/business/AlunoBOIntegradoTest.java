package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOIntegradoTest {

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 tera 16
	 * anos. Caso de teste 
	 * # | entrada 				 | saida esperada 
	 * 1 | aluno nascido em 2003 |	 * 16
	 */
	@Test
	public void testarCalculoIdadeAluno1() {
		
		AlunoBO alunoBO;
		
		AlunoDAO alunoDao = new AlunoDAO();
		
		AlunoBuilder alunoBuilder = AlunoBuilder.umAluno();
		
		Aluno aluno1 = alunoBuilder.comMatricula(2005678).deNome("Maria Santos").comSituacao(SituacaoAluno.ATIVO).nascidoEm(2003).build();
		
		alunoDao.salvar(aluno1);
		
		alunoBO = new AlunoBO(alunoBuilder.umAlunoDAO(aluno1), new DateHelper());
		
		Integer idade = alunoBO.calcularIdade(aluno1.getMatricula());
		
		Assertions.assertEquals(16, idade);
		
	}

	/**
	 * Verificar se alunos ativos sao atualizados.
	 */
	public void testarAtualizacaoAlunosAtivos() {
	}

}
